import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const loadState = () => {
  try {
    const serializedState = localStorage.getItem('vue_state')
    if (serializedState === null) {
      return undefined
    }
    return JSON.parse(serializedState)
  } catch (err) {
    return undefined
  }
}

const saveState = (state) => {
  try {
    const serializedState = JSON.stringify(state)
    localStorage.setItem('vue_state', serializedState)
  } catch (err) {
    console.error(`Something went wrong: ${err}`)
  }
}
const todosList = [
  {
    id: 1,
    title: 'learn vue',
    done: false,
    liked: false
  },
  {
    id: 2,
    title: 'learn vuex',
    done: false,
    liked: false
  }
]

export default new Vuex.Store({
  state: {
    todos: loadState() || todosList
  },
  mutations: {
    ADD_TASK (state, task) {
      const itemTask = {
        id: Math.floor(Math.random() * 100) + 1,
        title: task,
        done: false,
        liked: false
      }
      state.todos.push(itemTask)
      saveState(state.todos)
    },
    DEL_TASK (state, task) {
      const index = this.state.todos.indexOf(task)
      state.todos.splice(index, 1)
      saveState(state.todos)
    },
    UPDATE_TASK (state, task) {
      const index = this.state.todos.indexOf(task.todo)
      const itemTask = {
        id: task.todo.id,
        title: task.newTask,
        done: task.todo.done,
        liked: task.todo.liked
      }
      state.todos.splice(index, 1, itemTask)
      saveState(state.todos)
    },
    COMPLETE_TASK (state, task) {
      Vue.set(state.todos, task.index, task.newTodo)
      saveState(state.todos)
    },
    LIKE_TASK (state, task) {
      debugger
      Vue.set(state.todos, task.index, task.newTodo)
      saveState(state.todos)
    }
  }
})
