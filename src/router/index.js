import Vue from 'vue'
import Router from 'vue-router'
import ListManager from '../components/ListManager'
import Details from '../components/Details'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'ListManager',
      component: ListManager
    },
    {
      path: '/task/:id',
      name: 'Details',
      component: Details
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
